package dk.camelot64.kickass.xexplugin;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import kickass.plugins.interf.general.IEngine;
import kickass.plugins.interf.general.IMemoryBlock;
import kickass.plugins.interf.general.IParameterMap;
import kickass.plugins.interf.general.IValue;
import kickass.plugins.interf.segmentmodifier.ISegmentModifier;
import kickass.plugins.interf.segmentmodifier.SegmentModifierDefinition;

import static dk.camelot64.kickass.xexplugin.AtariXex.addBytes;
import static dk.camelot64.kickass.xexplugin.AtariXex.addWord;
import static dk.camelot64.kickass.xexplugin.AtariXex.toByteArray;

/**
 * Kick Assembler Segment modifier for creating Atari 8bit XEX file format output.
 * The XEX format is a binary executable format containing multiple memory blocks.
 * See https://www.atarimax.com/jindroush.atari.org/afmtexe.html
 * <p>
 * The segment modifier is used through the <i>modify</i> segment parameter.
 * It accepts the parameter <i>_RunAddr</i>, which sets the address for starting the program (either using an integer or a label).
 * <p>
 * <code>
 * .plugin "dk.camelot64.kickass.xexplugin.AtariXex"<br>
 * .file [name="plugin.xex", type="bin", segments="Program", modify="XexFormat", _RunAddr=start]
 * </code>
 * <p>
 * Where multiple segments are defined, the order in which they are loaded can be specified with the <i>_Order</i>
 * parameter. This is only of benefit when in combination with XexInit segment modifier, to force loading of blocks
 * of code in a specific order, and then immediately run an INIT routine after loading a particular block.
 * If no order is set segments are loaded in their natural ascending memory address order.
 * <p>
 * <code>
 * .segmentdef XexFile [segments="Program", modify="XexFormat", _RunAddr=start, _Order="Code2, Code1"]<br>
 * .segmentdef Program [segments="Code1, Code2", modify="XexInit"]<br>
 * .segmentdef Code1 [start=$2000, modify="XexInit", _InitAddr=load1]<br>
 * .segmentdef Code2 [start=$3000, modify="XexInit", _InitAddr=load2]<br>
 * </code>
 */
public class XexFormatSegmentModifier implements ISegmentModifier {

    private final InitBlockManager initBlockManager;

    public XexFormatSegmentModifier(InitBlockManager initBlockManager) {
        this.initBlockManager = initBlockManager;
    }

    @Override
    public SegmentModifierDefinition getDefinition() {
        final SegmentModifierDefinition definition = new SegmentModifierDefinition();
        definition.setName("XexFormat");
        final HashSet<String> paramNames = new HashSet<>();
        paramNames.add("_RunAddr");
        paramNames.add("_Order");
        definition.setAllParameters(paramNames);
        return definition;
    }

    @Override
    public List<IMemoryBlock> execute(List<IMemoryBlock> memoryBlocks, IParameterMap parameters, IEngine engine) {
        List<Byte> xexBytes = new ArrayList<>();

        // Start by marking the XEX as binary
        addWord(xexBytes, 0xffff);
        // Rearrange the memory blocks into specified order if given
        List<IMemoryBlock> arrangedBlocks = reorderMemoryBlocks(memoryBlocks, parameters);
        // Loop through all memory blocks and output each one separately, dealing with any INIT addresses
        for (IMemoryBlock memoryBlock : arrangedBlocks) {
            final int blockStartAddress = memoryBlock.getStartAddress();
            final byte[] blockBytes = memoryBlock.getBytes();
            int blockEndAddress = blockStartAddress + blockBytes.length - 1;
            addWord(xexBytes, blockStartAddress);
            addWord(xexBytes, blockEndAddress);
            addBytes(xexBytes, blockBytes);
            // add any INITADDR the manager tells us are needed for this block and all the blocks processed so far
            addBytes(xexBytes, initBlockManager.getInitBytes(memoryBlock));
        }

        // Clear the init blocks - preparing for any additional compilations
        initBlockManager.clear();

        // Finally add any RUNADDR block
        if (parameters.exist("_RunAddr")) {
            final IValue runAddrParam = parameters.getValue("_RunAddr");
            if (!runAddrParam.hasIntRepresentation())
                engine.error("_RunAddr must be an integer program start address", parameters.getSourceRange("_RunAddr"));
            int runAddr = runAddrParam.getInt();
            addWord(xexBytes, 0x02e0);
            addWord(xexBytes, 0x02e1);
            addWord(xexBytes, runAddr);
        }

        return Collections.singletonList(engine.createMemoryBlock("xex_format", 0, toByteArray(xexBytes)));
    }

    /**
     * Reorder all memory blocks according to the "_Order" parameter.
     * The "_Order" parameter has the following format <code>_Order="Code2, Code1"</code> where Code1 and Code2 are segment/memory block names.
     * If no order is set segments are loaded in their natural ascending memory address order.
     * If a partial order is set the remaining blocks not mentioned are added after the ordered ones.
     * @param memoryBlocks All memory blocks.
     * @param parameters The parameters - containing the "_Order parameter.
     * @return
     */
    private List<IMemoryBlock> reorderMemoryBlocks(List<IMemoryBlock> memoryBlocks, IParameterMap parameters) {
        if (!parameters.exist("_Order") || !parameters.getValue("_Order").hasStringRepresentation())
            return memoryBlocks;

        // get all the block names which are default ordered by ascending memory location
        Stream<String> blockNames = memoryBlocks.stream().map(IMemoryBlock::getName);

        // Grab the specified order and convert it to a list, e.g. "A, B" to ["A", "B"]
        List<String> specifiedOrderedBlockNames = Stream.of(parameters.getValue("_Order").getString().split(","))
            .map(String::trim)
            .collect(Collectors.toList());

        // remove them from the rest of the block names, leaving everything else
        Stream<String> others = blockNames.filter(e -> !specifiedOrderedBlockNames.contains(e));

        // recombine in the specified order we want, start with the blocks specified in _Order
        Stream<String> combined = Stream.concat(specifiedOrderedBlockNames.stream(), others);

        // Add in the blocks not mentioned in _Order
        return combined.map(n -> memoryBlocks.stream().filter(b -> b.getName().equals(n)).findAny().orElse(null))
            .filter(Objects::nonNull)
            .collect(Collectors.toList());
    }

}
