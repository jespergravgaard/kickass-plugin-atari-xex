package dk.camelot64.kickass.xexplugin;

import kickass.plugins.interf.IPlugin;
import kickass.plugins.interf.archive.IArchive;

import java.util.ArrayList;
import java.util.List;

/** KickAssembler Plugin Collection for the XEX modifiers */
public class AtariXex implements IArchive {

    @Override
    public List<IPlugin> getPluginObjects() {
        final ArrayList<IPlugin> plugins = new ArrayList<>();
        final InitBlockManager initBlockManager = new InitBlockManager();
        XexInitSegmentModifier xexInitSegmentModifier = new XexInitSegmentModifier(initBlockManager);
        XexFormatSegmentModifier xexFormatSegmentModifier = new XexFormatSegmentModifier(initBlockManager);
        plugins.add(xexFormatSegmentModifier);
        plugins.add(xexInitSegmentModifier);
        return plugins;
    }

    /**
     * Convert list of Bytes to a byte[]
     *
     * @param byteList The list of Byte
     * @return A byte array
     */
    static byte[] toByteArray(List<Byte> byteList) {
        byte[] bytes = new byte[byteList.size()];
        int i = 0;
        for (Byte aByte : byteList)
            bytes[i++] = aByte;
        return bytes;
    }

    /**
     * Add a byte value to the List of Byte.
     *
     * @param byteList The List of Byte
     * @param theByte  The byte value to add
     */
    static void addByte(List<Byte> byteList, int theByte) {
        byteList.add((byte) (theByte & 0xff));
    }

    /**
     * Add a word value to a List of Byte.
     * Words are added using little endian
     *
     * @param byteList The XEX bytes
     * @param word     The word value to add
     */
    static void addWord(List<Byte> byteList, int word) {
        byteList.add((byte) (word & 0xff));
        byteList.add((byte) ((word >> 8) & 0xff));
    }

    /**
     * Add a byte array to the List of Byte.
     *
     * @param byteList The List of Byte
     * @param bytes    The bytes array value to add
     */
    static void addBytes(List<Byte> byteList, byte[] bytes) {
        for(byte blockByte : bytes)
            addByte(byteList, blockByte);
    }

}
