package dk.camelot64.kickass.xexplugin;

import java.util.HashSet;
import java.util.List;
import kickass.plugins.interf.general.IEngine;
import kickass.plugins.interf.general.IMemoryBlock;
import kickass.plugins.interf.general.IParameterMap;
import kickass.plugins.interf.segmentmodifier.ISegmentModifier;
import kickass.plugins.interf.segmentmodifier.SegmentModifierDefinition;

/**
 * Kick Assembler Segment modifier "XexInit" for adding a Atari 8bit XEX INIT segment to a code segment.
 * The "XexFormat" modifier must still be used for converting the file to XEX and the "XexInit" can then be added to selected segments to add initializers.
 * <p>
 * From https://www.atarimax.com/jindroush.atari.org/afmtexe.html
 * After each segment is loaded, word at location $02E2 is checked to see if it was filled in with an INIT address. If so, that location is called as a subroutine before processing any additional segments.
 * <p>
 * The segment modifier is used through the <i>modify</i> segment parameter.
 * The segment modifier accepts the parameter <i>_InitAddr</i>, which sets the INIT address to run when the segment is loaded.
 * <p>
 * <code>
 *   .plugin "dk.camelot64.kickass.xexplugin.AtariXex"<br>
 *   .file [name="initAddr.xex", type="bin", segments="XexFile"]<br>
 *   .segmentdef XexFile [segments="Program", modify="XexFormat", _RunAddr=start]<br>
 *   .segmentdef Program [segments="Code1, Code2" ]<br>
 *   .segmentdef Code1 [start=$2000, modify="XexInit", _InitAddr=load1]<br>
 *   .segmentdef Code2 [start=$3000, modify="XexInit", _InitAddr=load2]<br>
 * </code>
 */
public class XexInitSegmentModifier implements ISegmentModifier {

    private final InitBlockManager initBlockManager;

    public XexInitSegmentModifier(InitBlockManager initBlockManager) {
        this.initBlockManager = initBlockManager;
    }

    @Override
    public SegmentModifierDefinition getDefinition() {
        final SegmentModifierDefinition definition = new SegmentModifierDefinition();
        definition.setName("XexInit");
        final HashSet<String> paramNames = new HashSet<>();
        paramNames.add("_InitAddr");
        definition.setAllParameters(paramNames);
        return definition;
    }

    @Override
    public List<IMemoryBlock> execute(List<IMemoryBlock> memoryBlocks, IParameterMap parameters, IEngine engine) {
        if (memoryBlocks.size() > 0 && parameters.exist("_InitAddr") && parameters.getValue("_InitAddr").hasIntRepresentation()) {
            initBlockManager.addInit(memoryBlocks, parameters.getValue("_InitAddr").getInt());
        }
        return memoryBlocks;
    }
}
