package dk.camelot64.kickass.xexplugin;

import kickass.plugins.interf.general.IMemoryBlock;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static dk.camelot64.kickass.xexplugin.AtariXex.addWord;
import static dk.camelot64.kickass.xexplugin.AtariXex.toByteArray;

/**
 * Keeps track of INIT blocks that have to be added to the XEX file.
 *
 * The INIT blocks are added by the {@link XexInitSegmentModifier} and read by the {@link XexFormatSegmentModifier}
 */
public class InitBlockManager {
    /** This keeps track of the names of all memory blocks already added to the output. */
    private final List<String> blocksProcessed = new ArrayList<>();
    /** This is all init blocks to add to the output. */
    private final List<InitBlock> initBlocks = new ArrayList<>();

    /**
     * Add an INITADDR that will be added to the output after some memory blocks.
     * This is called whenever a KickAss segment with <code>modify="XexInit", _InitAddr=load1</code> is encountered in the source.
     * @param memoryBlocks The memory blocks to add the init block after. This is all blocks of the segment with the XexInit modifier.
     * @param initAddr The address to add to the INITADDR. This is the address pointed to by the _InitAddr parameter.
     */
    public void addInit(List<IMemoryBlock> memoryBlocks, int initAddr) {
        // Sadly we don't get the name "Program", so we have to make a name up. Just join the segments names together.
        String name = memoryBlocks.stream().map(InitBlockManager::getBlockName).collect(Collectors.joining(","));
        // Now get the names of the blocks as our dependencies, e.g. "Code1", "Code2"
        List<String> dependencies = memoryBlocks.stream().map(InitBlockManager::getBlockName).collect(Collectors.toList());
        // Create the init block for this "multi-part" segment
        InitBlock initBlock = new InitBlock(name, dependencies, initAddr);
        initBlocks.add(initBlock);
    }

    /**
     * Get bytes representing INITADDR blocks to be appended after a memory block
     * @param name The name of the memory block
     * @return Array of bytes containing any INITADDR blocks to be added after the memory block.
     */
    public byte[] getInitBytes(IMemoryBlock memoryBlock) {
        final String blockName = getBlockName(memoryBlock);
        blocksProcessed.add(blockName);
        List<Byte> output = new ArrayList<>();
        // Are there any unprocessed multi-segment blocks that have all their dependent segments complete?
        for(InitBlock initBlock: initBlocks) {
            if (!initBlock.processed && blocksProcessed.containsAll(initBlock.dependencies)) {
                // we can output this multi-segment block
                addWord(output, 0x02e2);
                addWord(output, 0x02e3);
                addWord(output, initBlock.initAddress);
                // mark it as done
                initBlock.processed = true;
            }
        }
        return toByteArray(output);
    }

    /**
     * Clear the managed blocks.
     */
    public void clear() {
        initBlocks.clear();
        blocksProcessed.clear();
    }

    /**
     * Get a unique memory block name usable for identifying the memory block.
     * @param memoryBlock The memory block
     * @return The name
     */
    public static String getBlockName(IMemoryBlock memoryBlock) {
        return memoryBlock.getName()+"("+memoryBlock.getStartAddress()+")";
    }

    // A helper class for storing related information about the block. Only used internally by the manager itself.
    private static class InitBlock {
        String name;
        List<String> dependencies;
        int initAddress;
        boolean processed;

        InitBlock(String name, List<String> dependencies, int initAddress) {
            this.name = name;
            this.dependencies = dependencies;
            this.initAddress = initAddress;
            processed = false;
        }
    }

}
