package dk.camelot64.kickass.xexplugin;

import kickass.plugins.impl.PluginMemoryBlock;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;

import static dk.camelot64.kickass.xexplugin.TestUtil.asByteArray;
import static org.junit.jupiter.api.Assertions.assertArrayEquals;

class InitBlockManagerTest {

    @Test
    void shouldReturnSingleSegmentBlockData() {
        InitBlockManager manager = new InitBlockManager();
        PluginMemoryBlock block1 = new PluginMemoryBlock("block1", 0x1234, asByteArray(Arrays.asList(0xa9, 0x00)));
        PluginMemoryBlock block2 = new PluginMemoryBlock("block2", 0x9abc, asByteArray(Arrays.asList(0xa9, 0x01)));

        // Given single blocks are given to manager
        manager.addInit(Collections.singletonList(block1), 0x2345);
        manager.addInit(Collections.singletonList(block2), 0x3456);

        // When I ask for the same named init block, i get the init vectors and block's init address
        assertArrayEquals(asByteArray(Arrays.asList(0xe2, 0x02, 0xe3, 0x02, 0x45, 0x23)), manager.getInitBytes(block1));
        assertArrayEquals(asByteArray(Arrays.asList(0xe2, 0x02, 0xe3, 0x02, 0x56, 0x34)), manager.getInitBytes(block2));
    }

    @Test
    void shouldReturnMultiSegmentBlockAfterAllDependentSingleBlockData() {
        InitBlockManager manager = new InitBlockManager();
        PluginMemoryBlock block1 = new PluginMemoryBlock("block1", 0x1234, asByteArray(Arrays.asList(0xa9, 0x00)));
        PluginMemoryBlock block2 = new PluginMemoryBlock("block2", 0x9abc, asByteArray(Arrays.asList(0xa9, 0x01)));

        // Given single blocks are given to manager
        manager.addInit(Collections.singletonList(block1), 0x2345);
        manager.addInit(Collections.singletonList(block2), 0x3456);
        // and a multi-segment block is given to the manager
        manager.addInit(Arrays.asList(block1, block2), 0x4567);

        // When I ask for the one of the single blocks (Out of Order), i get just its data
        assertArrayEquals(asByteArray(Arrays.asList(
            0xe2, 0x02, 0xe3, 0x02, 0x56, 0x34  // block 2
        )), manager.getInitBytes(block2));

        // then when I ask for the other block, i get it, and also the multi block that depends on the 2 blocks output
        assertArrayEquals(asByteArray(Arrays.asList(
            0xe2, 0x02, 0xe3, 0x02, 0x45, 0x23, // block 1
            0xe2, 0x02, 0xe3, 0x02, 0x67, 0x45  // multi-segment init address
        )), manager.getInitBytes(block1));
    }

    @Test
    void shouldClearData() {
        InitBlockManager manager = new InitBlockManager();
        PluginMemoryBlock block1 = new PluginMemoryBlock("block1", 0x1234, asByteArray(Arrays.asList(0xa9, 0x00)));

        // Given a single block added
        manager.addInit(Collections.singletonList(block1), 0x1234);

        // Then I can get the data out
        assertArrayEquals(asByteArray(Arrays.asList(0xe2, 0x02, 0xe3, 0x02, 0x34, 0x12)), manager.getInitBytes(block1));

        // But when I clear the manager
        manager.clear();

        // Then asking for the block returns an empty array
        assertArrayEquals(new byte[0], manager.getInitBytes(block1));
    }

    @Test
    void shouldInitOnSecondBlock() {
        InitBlockManager manager = new InitBlockManager();
        PluginMemoryBlock block1 = new PluginMemoryBlock("block1", 0x1234, asByteArray(Arrays.asList(0xa9, 0x00)));
        PluginMemoryBlock block2 = new PluginMemoryBlock("block2", 0x9abc, asByteArray(Arrays.asList(0xa9, 0x01)));

        // Given only a multi-segment block is given to the manager
        manager.addInit(Arrays.asList(block1, block2), 0x4567);

        // Asking for the first block returns an empty array
        assertArrayEquals(new byte[0], manager.getInitBytes(block1));
        // Asking for the second block, i get the init address of the multi-segment block
        assertArrayEquals(asByteArray(Arrays.asList(0xe2, 0x02, 0xe3, 0x02, 0x67, 0x45)), manager.getInitBytes(block2));
    }
}