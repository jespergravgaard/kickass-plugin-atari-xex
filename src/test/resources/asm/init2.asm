// Tests the XEX plugin - including INIT address for an outer segment
.plugin "dk.camelot64.kickass.xexplugin.AtariXex"
.file [name="init2.xex", type="bin", segments="File"]
.segmentdef File [segments="Program", modify="XexFormat", _RunAddr=start]
.segmentdef Program [segments="Code1, Code2", modify="XexInit", _InitAddr=load1 ]
.segmentdef Code1 [start=$2000]
.segmentdef Code2 [start=$3000]

.segment Code1
start:
    lda #0
    sta $8000
load1:
    lda #1
    sta $8001
    rts

.segment Code2
    lda #3
    sta $8003
    rts
load2:
    lda #2
    sta $8002
    rts
