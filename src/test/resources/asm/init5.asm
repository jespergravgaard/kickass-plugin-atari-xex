// Tests the XEX plugin
// Segment order is Code2, Code1, with their init blocks after they load, then Program Init, then Run address
.plugin "dk.camelot64.kickass.xexplugin.AtariXex"
.file [name="init5.xex", type="bin", segments="File"]
.segmentdef File [segments="Program", modify="XexFormat", _RunAddr=start, _Order="Code2, Code1"]
.segmentdef Program [segments="Code1, Code2", modify="XexInit", _InitAddr=load3]
.segmentdef Code1 [start=$2000, modify="XexInit", _InitAddr=load1]
.segmentdef Code2 [startAfter="Code1", modify="XexInit", _InitAddr=load2]

.segment Code1
start:
    lda #0
    sta $8000
load1:
    lda #1
    sta $8001
    rts

.segment Code2
load2:
    lda #2
    sta $8002
    rts
load3:
    lda #3
    sta $8001
    rts
