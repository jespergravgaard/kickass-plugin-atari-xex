// Tests the XEX plugin - combining INIT and .PC
.plugin "dk.camelot64.kickass.xexplugin.AtariXex"
.file [name="init7.xex", type="bin", segments="File"]
.segmentdef File [segments="Program", modify="XexFormat", _RunAddr=start]
.segmentdef Program [segments="Code1, Code2" ]
.segmentdef Code1 [start=$2000, modify="XexInit", _InitAddr=load1]
.segmentdef Code2 [start=$3000, modify="XexInit", _InitAddr=load2]

.segment Code1
start:
    lda #0
    sta $8000
    rts
.pc = $2500
load1:
    rts
.segment Code2
    lda #2
    sta $8002
    rts
.pc = $3500
load2:
    rts
