// Tests the XEX plugin
// Using .pc to create a second memory block
.plugin "dk.camelot64.kickass.xexplugin.AtariXex"
.file [name="pc.xex", type="bin", segments="File"]
.segmentdef File [segments="Program", modify="XexFormat"]
.segmentdef Program [start=$2000]
.segment Program

lda #0
sta $d020
rts

.pc = $3000
lda #0
sta $d020
rts